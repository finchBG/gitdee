﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMechanics : MonoBehaviour
{
    [SerializeField] private Transform groundCheckTransform = null;
    [SerializeField] private LayerMask playerMask;
    private bool jumpButtonPressed;
    private Rigidbody rigidBodyComponent;
    private int superJumpsRemaining;
    public float movementSpeed = 0.1f;
    public float maxMovementSpeed = 0.1f;
    public float accelerationSpeed = 0.1f;
    public float decelerationSpeed = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        rigidBodyComponent = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpButtonPressed = true;
        }
    }
    // fixedUpd called once every physics upd
    private void FixedUpdate()
    {
        if (Physics.OverlapSphere(groundCheckTransform.position, 0.1f, playerMask).Length == 0)
        {
            return;
        }

        if (jumpButtonPressed)
        {
            float jumpPower = 6;

            if (superJumpsRemaining > 0)
            {
                jumpPower *= 1.2f;
                superJumpsRemaining--;
            }

            rigidBodyComponent.AddForce(Vector3.up * jumpPower, ForceMode.VelocityChange);
            jumpButtonPressed = false;
        }

        if (movementSpeed <= maxMovementSpeed)
        {
            Vector3 horizontalInput = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            rigidBodyComponent.velocity = Vector3.MoveTowards(rigidBodyComponent.velocity, horizontalInput * movementSpeed, Time.deltaTime * accelerationSpeed);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9)
        {
            Destroy(other.gameObject);
            superJumpsRemaining++;
        }
    }
}
